#ifndef  DATASTRUCTURE_ALGORITHM_HPP
#define DATASTRUCTURE_ALGORITHM_HPP

#include<vector>
#include<iostream>

void swap(int& a, int& b)
{
	int temp = a;
	a = b;
	b = temp;
}

// Question 1
void quickSort(int left, int right, std::vector<int>& arr)
{
	if (left > right)
	{
		return;
	}
	int i, j;
	i = left;
	j = right;
	int base = arr[left]; //value of the pivot; choose the first element to be pivot
	while (i < j)
	{
		// find the first two elements need to be changed the position
		while (arr[j] >= base && i < j)
		{
			j--;
		}
		while (arr[i] <= base && i < j)
		{
			i++;
		}
		if (i < j)
		{
			swap(arr[i], arr[j]);
		}
	}
	// end when i = j
	// since the j-- is before i ++ arr[i]<=base
	arr[left] = arr[i];
	arr[i] = base;
	//recursion
	quickSort(left, i - 1, arr);
	quickSort(i + 1, right, arr);
}

void QuickSortMethod(std::vector<int>& arr)
{
	int size = arr.size();
	quickSort(0, size - 1, arr);
}


// P2
template<typename T>
class heap
{
private:
	std::vector<T> elements;
	int size;
public:
	heap()
	{
		size = 0;
	}
	void Add(const int& value)
	{
		//add the new element in the tail
		elements.push_back(value);
		size++;
		if (size == 1) return;
		int i = size - 1;
		while (i > 0)
		{
			int i_parent = (i + 1) / 2 - 1;
			if (elements[i] > elements[i_parent])
			{
				swap(elements[i], elements[i_parent]);;
				i = i_parent;
			}
			else
			{
				break;
			}
		}
	}

	void print() const
	{
		for (auto i : elements)
		{
			std::cout << " " << i;
		}
		std::cout << std::endl;
	}

	//question 3
	void pop()
	{
		//move the last elements in the first place
		elements[0] = elements[size - 1];
		int i = 0;
		size--;
		while (2 * (i + 1) - 1 < size)
		{
			if (2 * (i + 1) < size && elements[2 * (i + 1) - 1] > elements[2 * (i + 1)])
			{
				if (elements[2 * (i + 1) - 1] > elements[i])
				{
					swap(elements[2 * (i + 1) - 1], elements[i]);
					i = 2 * (i + 1) - 1;
				}
				else break;
			}
			if (2 * (i + 1) < size && elements[2 * (i + 1) - 1] < elements[2 * (i + 1)])
			{
				if (elements[2 * (i + 1) ] > elements[i])
				{
					swap(elements[2 * (i + 1)], elements[i]);
					i = 2 * (i + 1);
				}
				else break;
			}
			if (2 * (i + 1) >= size)
			{
				if (elements[2 * (i + 1) - 1] > elements[i])
				{
					swap(elements[2 * (i + 1)-1], elements[i]);
					i = 2 * (i + 1)-1;
				}
				else break;
			}
		}
	}
};

#endif // ! DATASTRUCTURE_ALGORITHM_HPP
